package app.com.purchasemodule;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;


import java.util.List;

/**
 * <p>Класс - модуль для работы с покупками в приложении</p>
 * <p>С его помощью достаеться информация про товары в приложении, происходит покупка, потребление
 * товаров и подписок, а также валидация</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 */
public class PurchaseModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "PURCHASE_MODULE";
    public static final String BASE64_PURCHASE_KEY = "key from google play";
    public static final String VALIDATOR_URL = "url";


    private Activity activity;

    public PurchaseModule(Activity _activity){
        activity = _activity;
    }

    private void init(final List<String> skus){

    }

    private void buy(final String sku, final String developerPayload){

    }

    private void subscribe(final String sku, final String developerPayload, final List<String> oldPurchasedSkus){

    }

    private JSONArray getAvailableProducts(){
        return null;
    }

    private JSONArray getProductDetails(final List<String> skus){
        return  null;
    }

    private void ConsumeProducts(JSONArray arr){

    }

    public void Dispose(){

    }
    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */
    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */
    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }

}
